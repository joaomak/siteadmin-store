 Software
===========================================================
**siteAdmin** is a flatfile CMS package that works 
fast and efficient with a rich and friendly interface.
Official Website - <http://www.getsiteadmin.com/>

siteAdmin was developed by Joao Makray (<http://www.joaomak.net>)
Oktala Communication & System Development (<http://www.oktala.com>)

 Disclaimer
===========================================================

This software is provided strictly on an as is basis. 
You are soley responsible and liable for any damage you do 
to your website or server. If you are unsure how to install 
or operate any server side software pleace contact your 
server administrator.

 Documentation & Support
===========================================================

Available at <http://www.getsiteadmin.com>
