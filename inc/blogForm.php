<hr />
<h4><?php echo $commentTag[$lang]; ?>:</h4>
<form method="post" action="post.html" id="comment-form" accept-charset="UTF-8" onsubmit="return checkform(this)">  
    
	<p>
	  <label for="name"><span><?php echo $yourNameTag[$lang]; ?>:</span></label>
        <input id="name" name="name" type="text" size="30" class="required" />		
		
	</p>
	<p>
	  <label for="email"><span><?php echo $yourEmailTag[$lang]; ?>: <small>(<?php echo $privacyEmailTag[$lang]; ?>)</small></span> </label>
        <input id="email" name="email" type="text" size="30" />
	  		
	</p>
	<p>
	  <label for="url"><span>Website: <small>(<?php echo $optionalTag[$lang]; ?>)</small></span></label>
        <input id="url" name="url" type="text" size="30" />	  
		
	</p>
	<p>
        <label for="text"><span><?php echo $yourComment[$lang]; ?>:</span> </label>
	  <textarea cols="48" rows="6" id="text" name="text" class="required"></textarea> 
        
	</p>
	<p class="submit">
	  <input value="<? echo $_GET['link']; ?>" name="link" type="hidden" />
	  <input value="<?php echo $publishTag[$lang]; ?>" id="preview" name="preview" class="btn" type="submit" />         
		
	</p>
</form>

