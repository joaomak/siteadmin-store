<?php
/************************************************\

  Developed by Oktala <www.oktala.com.br>

\************************************************/
session_start();
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
header('Content-Type: text/html; charset=utf-8');
include 'siteadmin/lib/functions.php';
include 'siteadmin/lib/webElements.php';
include 'inc/lang.php';
date_default_timezone_set('America/Sao_Paulo');

$description = '';
$rootTemplate = 'main.php';
$base=$_SERVER['REQUEST_SCHEME'].'://'. stripslashes($_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']));if(dirname($_SERVER['PHP_SELF'])!='/')$base.= '/';
// JS
$js=array();
$js[0]='//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js';
$js[1]='//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js';
//$js[7]='inc/parallax.min.js'; //class=parallax-window&data-parallax=scroll&data-image-src=
$js[10]='inc/functions.min.js';
// CSS
$css='';
$cssf=array();
$cssf[]='//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css';
$cssf[]='inc/style.min.css';
//$cssf[]='inc/custom.css';

function valor($str, $number=false){
	if ($number) return number_format(floatval(str_replace(',','.',$str)), 2, '.', '');
	return 'R$ '.number_format(floatval(str_replace(',','.',$str)), 2, ',', '.');
}
function supTitle ($t){
	global $lang;
	$t=saDecode($t);
	$ex=explode('|',$t);
	if(count($ex)>1){
		if($lang=='pt')$t=trim($ex[0]);
		else $t=trim($ex[1]);
	}
	return $t;
}
/************************************************/
if(!isset($_GET['pg'])||$_GET['pg']=='')$pg="home";
else $pg=$_GET['pg'];
$content='';
$related='';
$carousel='';
$feature='';
$default=false;
$paginas=get('Menu_produtos');

$config=get(saEncode('Configurações'));
$fixedTitle = ' | '.$config['nome'];
$body=$config['template_extra'];
$head_extra=$config['head_extra'];
if($config['logo']){
	$logo='<img src="'.$config['logo'].'"  ';
	$feature=$config['logo'];
	if($config['logo_mobile']){
		$logo.='srcset="'.$config['logo_mobile'].' 2x" ';
		$feature=$config['logo_mobile'];
	}
	$logo.='alt="Logo" class="img-responsive" />';
}
if($config['padrao_facebook']) $feature=$config['padrao_facebook'];
if(trim($config['mensagem'])){
	$mensagem=div('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> '.$config['mensagem'], 'class=alert alert-warning text-center mensagem');
}

switch($pg){
	case "home":
		$title='';
		$fixedTitle = substr($fixedTitle, 3);

		$c=get('Home');
		$fixedTitle ='';
		$title=$c['titulo'];
		$description=$c['descricao'];
		//$content .= h1($title);
		if($c['texto']) $content .= br().div(responsive($c['texto']), 'class=lead');

		if($c['destaques']){
			$car=new carousel();
			$fotos=files($c['destaques']);
			shuffle($fotos);
			foreach ($fotos as $key => $value) {
				$img = img('class=img-responsive center-block&src=size1600crop3-1/'.$c['destaques'].'/'.$value);
				if(isset($c['list'][$value])){
					$fileInfo=$c['list'][$value];
					if($fileInfo['ocultar']=='1')continue; //ocultar
					if($fileInfo['texto'])$img.=div($fileInfo['texto'],'class=destaque');
					if($fileInfo['link']){
						$it=a(
							$img
						,'href='.$fileInfo['link']);
					}else $it=$img;
					$car->add($it);
				}else $car->add($img);
			}
			$carousel.=$car->get_html();
		}

	break;
	case 'about-us':
	case 'quemsomos':
	case 'quem-somos':
		$c=get('Quem_Somos');
		$title=$c['titulo'];
		$content.=h1($title);
		$r=new div('class=row text-left');
		if($c['foto']){
			$feature='thumb900/'.str_urlencode($c['foto']);
			$r->add(div(img('class=img-responsive&src=thumb500/'.$c['foto']), 'class=col-sm-5'));
			$r->add(div(responsive($c['texto']), 'class=col-sm-7'));
		}else{
			$r->add(div(responsive($c['texto']), 'class=col-sm-12'));
		}
		$content.= $r->get_html();
		$c=get('Equipe');
		$div=new div('class=row&id=equipe');
		foreach($c as $member){
			if($member['ocultar']) continue;
			$item='';
			if($member['foto'])$item.=img('class=img-responsive img-circle&src=size200crop1-1/'.$member['foto']);
			$item.=h3($member['nome']);
			$item.=($member['bio']);
			$div->add(div($item, 'class=col-xs-6 col-sm-4'));
		}
		if(!$div->empty) $content.=hr().$div->get_html().br();
	break;
	case 'post':
		$b=pages('NotiACUTEcias');
		if(isset($_GET['link']) && isset($b[$_GET['link']]) && file_exists('data/NotiACUTEcias.sup/'.$b[$_GET['link']].'.one')){
			$c=get('NotiACUTEcias.sup/'.$b[$_GET['link']].'.one');
			//print_r($c);
			$title= $c['titulo'];
			$description=$c['descricao'];
			//if(isset($c['meta_keywords']) && $c['meta_keywords'])$keywords=$c['meta_keywords'];
			if($c['foto']){
				$feature='thumb900/'.str_urlencode($c['foto']);
				$content.=p(img('class=img-responsive center-block&src=thumb1016/'.str_urlencode($c['foto'])));
			}
			$content.=h1($c['titulo']);
			$content.=p(dateFormat($c['data'],$i18n['date_format'], $lang), 'class=data');
			$content.=div(responsive($c['texto']),'class=text-left');
		}
		$content.=br().p(a('<i class="glyphicon glyphicon-chevron-left"></i> '.$i18n['view_news'], 'href=noticias&class=btn btn-default')).br();
	break;
	case "news":
	case "noticias":
		$pages=pages('NotiACUTEcias');
		$title=$i18n['noticias'];
		$related.=h2($title, 'class=text-center');
		$related.='<div class="row news-list">';
		$related.='<div class="col-md-8 col-md-offset-2">';
		$count=0;
		foreach ($pages as $key => $value) {
			$c=get('NotiACUTEcias.sup/'.$value);
			$link='post/'.$key;
			$d=new div('class=item');
			if($c['foto']) $d->add(p(a(img('class=img-responsive center-block&src=size960crop2-1/'.$c['foto']),'href='.$link)));
			if($c['data']) $d->add(small(dateFormat($c['data'],$i18n['date_format'], $lang),'class=pull-right'));
			$d->add(h4(a($c['titulo'],'class=blog-title&href='.$link)));
			$ps=explode('</p>',$c['texto']);
			$d->add($ps[0]);
			$related.=$d->get_html();
			$count++;
			if($count>=20)break;
		}
		$related.='</div>';
		$related.='</div>';
	break;

	case "photo-gallery":
	case "photos":
	case "gallery":
	case "fotos":
	case "galeria":
	case "galeria-de-fotos":
		$title = $i18n['fotos'];
		$content.=h1($title);
		$pages=pages('Fotos');
		$div=new div('class=albuns');
		foreach ($pages as $link=>$album){
			$c=get('Fotos.sup/'.$album);
			if($c['ocultar']=='1')continue;
			if($c['capa']){
				$item=img('src=size150crop1-1/'.$c['capa']);
				$item.=div($c['titulo'],'class=caption');
				$div->add(a($item, 'class=thumbnail&href=album/'.$link));
			}
		}
		$content.=$div->get_html();
	break;
	case "album":
		//$js[5]='inc/imagelightbox.min.js';
		//$cssf[]='inc/imagelightbox.min.css';
		$js[5]='inc/lightgallery.js/js/lightgallery.min.js';
		$cssf[]='inc/lightgallery.js/css/lightgallery.min.css';

		$pages=pages('Fotos');
		if(isset($_GET['link']) && isset($pages[$_GET['link']])){
			$c=get('Fotos.sup/'.$pages[$_GET['link']]);
			$title=$c['titulo'];
			$description=$c['descricao'];
			$feature='thumb900/'.str_urlencode($c['capa']);
			$content.=h1($title);
			if($c['texto']) $content.=$c['texto'];
			if($c['pasta']){
				$files=files($c['pasta']);
				$fit=new div('class=row album fit&id=album1');
				foreach($files as $file){
					$src= $c['pasta'].$file;
					$cleanFilename=explode('.',$file);
					array_pop($cleanFilename);
					$caption=str_replace('_',' ',implode('.',$cleanFilename));
					$fit->add(div(
						a(img('class=img-responsive&src=thumb500/'.$src.'?width\=300'), 'class=thumbnail&href=thumb900/'.$src.'&data-sub-html='.$caption)
					,'class=col-xs-6 col-sm-4 col-md-3'));
				}
				$content.=$fit->get_html();
			}else{
				$content.=p('Nenhuma foto aqui ainda...');
			}
			$content.=br().p(a('<i class="glyphicon glyphicon-chevron-left"></i> '.$i18n['all_photos'], 'href=fotos&class=btn btn-default')).br();
		}else{
			header("HTTP/1.0 404 Not Found");
			$title='404: '.$i18n['not_found'];
			$content=h1($i18n['not_found'], 'class=text-center').p('404', 'class=text-center').br().br();
		}
	break;

	case "produtos":
		//$js[5]='inc/imagelightbox.min.js';
		//$cssf[]='inc/imagelightbox.min.css';
		$js[5]='inc/lightgallery.js/js/lightgallery.min.js';
		//$js[6]='inc/lightgallery.js/js/lg-thumbnail.min.js';
		$js[7]='inc/lightgallery.js/js/lg-video.min.js';
		$js[8]='inc/lightgallery.js/js/lg-autoplay.min.js';
		$cssf[]='inc/lightgallery.js/css/lightgallery.min.css';

		$categorias=pages('Produtos');
		if(isset($_GET['link']) && isset($categorias[$_GET['link']]) && file_exists('data/Produtos.sup/'.$categorias[$_GET['link']])){
			$title= supTitle($categorias[$_GET['link']]);
			foreach($paginas as $categoria){
				if($categoria['link']==$_GET['link']){
					$title=$categoria['categoria'];
					$description= $categoria['descricao'];
					$modo=$categoria['modo'];
					break;
				}
			}
			if($modo=='Ocultar'){
				header("HTTP/1.0 404 Not Found");
				$title='404: '.$i18n['not_found'];
				$content=h1($i18n['not_found'], 'class=text-center').p('404', 'class=text-center').br().br();
				break;
			}
			$content.=h1($title,'class=cat-title');
			if($description) $content.=p($description,'class=cat-descr');
			else $description=$title;
			$content.=br();
			$produtos=get('Produtos.sup/'.$categorias[$_GET['link']]);

			$div=new div('class=cat');
			$rel=new div('class=row');
			foreach($produtos as $k=>$c){
				if($c['ocultar']=='1')continue;
			//print_r($c);
				//$title= $c['titulo'];
				//$description=$c['descricao'];
				$anchor=title2link($c['titulo']);

				if($c['capa']){
					$src= $c['capa'];
					$caption=new div('class=caption');
					if($c['mais_fotos']){
						$files=files($c['mais_fotos']);
						foreach($files as $file){
							if(!is_file($c['mais_fotos'].$file))continue;
							if(($c['mais_fotos'].$file)==($c['capa']))continue;
							$caption->add(a(
								img('src=size60crop1-1/'.$c['mais_fotos'].$file.'&srcset=size120crop1-1/'.$c['mais_fotos'].$file.' 2x')
							,'class=thumbnail&href=thumb900/'.$c['mais_fotos'].$file));
						}
					}
					if(isset($c['video']) && $c['video']){
						$pa=parse_url($c['video']);
			    	if(isset($pa['query'])){
							parse_str($pa['query']);
							$video_cover='http://img.youtube.com/vi/'.$v.'/1.jpg';
							//$video_embed=div(iframe('class=embed-responsive-item&src=//www.youtube.com/embed/'.$v),'class=embed-responsive embed-responsive-16by9');
							$caption->add(a(
								//img('src='.$video_cover)
								img('src=inc/lightgallery.js/img/video-play.png&alt=play&width=28&height=28')
							,array('class'=>'thumbnail video', 'target'=>'_blank', 'href'=>$c['video'], 'style'=>'background-image:url('.$video_cover.')')));
						}
					}

					//$caption->add(script('lightGallery(document.getElementById("galeria'.$k.'"), {thumbnail:true});'));

					$row=new div('class=row item&id='.$anchor);
					$row->add(div(
							a(img('class=img-responsive center-block img-rounded&src=thumb620/'.$src), 'href=thumb900/'.$src)
							.$caption->get_html()
					,'class=col-sm-6 text-center album&id=album'.$k));
					$info=new div('class=col-sm-6');
					$info->add(h3(a($c['titulo'], 'href=produtos/'.$_GET['link'].'/'.$anchor),'class=product-title'));
					if($c['valor'] && $config['ecommerce']){
						$qtd=1;
						//$url='addtocart?itemId='.title2link($c['titulo']).'&amp;itemDescription='.urlencode($c['titulo']).'&amp;itemAmount='.valor($c['valor'],true).'&amp;itemQuantity='.$qtd.'&amp;itemWeight='.floatval($c['peso']);
						$desconto=explode('->',$c['valor']);
						$valorFinal=array_pop($desconto);
						if(count($desconto)>0) $info->add(h5(del(valor(array_pop($desconto)),'class=text-muted')));
						$active=true;
						if($c['estado']){
							$st=$i18n['stock'].': ';
							switch($c['estado']){
								case 'em estoque': $st.=icon('glyphicon glyphicon-ok').' '.strong($i18n['in_stock']);break;
								case 'sob encomenda': $st.=icon('glyphicon glyphicon-time').' '.strong($i18n['on_demand']);break;
								case 'indisponível': $active=false;$st.=icon('glyphicon glyphicon-remove-sign').' '.strong($i18n['suspended']);break;
							}

						}
						if($active){
							if(substr($valorFinal, 0,4)=='http'){ //only link mode
								$market=icon('glyphicon glyphicon-new-window');
								if (strpos($valorFinal, 'elo7.com.br') !== false) {
								    $market=img('src=img/elo7.png&alt=Elo7');
								}else if (strpos($valorFinal, '.mercadolivre.com.br') !== false) {
								    $market=img('src=img/ml.png&alt=Mercado Livre');
								}else if (strpos($valorFinal, '.colab55.com') !== false) {
								    $market=img('src=img/colab55.png&alt=Colab 55');
								}
								$info->add(p(a(strong(($i18n['comprar'])).' &nbsp;'.$market, array('class'=>'btn btn-primary btn-success btn-lg','target'=>'_blank', 'href'=>$valorFinal))));
							}else{
								$info->add(
									h4(valor($valorFinal), 'class=price').' '
									.pagSeguro($c['titulo'], valor($valorFinal,true), floatval($c['peso']), title2link($c['titulo']))
									//.a($i18n['addtocart'].' '.icon('glyphicon glyphicon-chevron-right'), 'class=btn btn-primary&href='.str_replace('=','\=',$url))
								);
								if($config['frete_gratis'] || isset($_GET['freeshipping'])) $info->add(p(strong($i18n['free_shipping'],'class=text-danger')));
								else $info->add(p(small($i18n['delivery'],'class=text-muted')));
							}
						}
						if(isset($st)) $info->add(p($st,'class=text-muted'));
					}
					$divide=explode('<hr />', $c['texto']);
					$info->add(responsive($divide[0]));
					//modal
					if(isset($divide[1]) && $divide[1]){
							$modal=new modal('vejamais'.$k);
							$modal->add_header(h4($c['titulo'],'class=modal-title'));
							$modal->add_body(responsive($divide[1]));
			        $info->add(p('<a role="button" data-toggle="modal" data-target="#vejamais'.$k.'" >'.$i18n['veja_mais'].'</a>'));
							$body.=($modal->get_html());
		      }
					$row->add($info->get_html());

					// produto
					if(isset($_GET['item']) && $_GET['item']==$anchor){
						$title=	$c['titulo'].' - '.$title;
						$description=$c['descricao'];
						$feature='thumb900/'.str_urlencode($c['capa']);
						$produto=$row->get_html();
					}else{
						$div->add($row->get_html());
						$rel->add(div(
							a(h4($c['titulo']), 'style=background-image:url(thumb250/'.$src.')&href=produtos/'.$_GET['link'].'/'.$anchor)
						,'class=col-sm-4 col-md-3 text-center'));
					}
				}
				if($modo=='Ícones'){
					$div=new div();
					$related='<div class="related">';
					$related.='<div class="container">';
					$related.=br();//h3($i18n['related'],'class=text-center').br();
					$related.=$rel->get_html();
					$related.='</div>';
					$related.='</div>';
				}
				//$content.=h2($c['titulo']);
				//$content.=div($c['texto'],'class=text-left');
			}
			if(isset($produto)){
				$content=div($produto, 'class=cat');
				$related='<div class="related">';
				$related.='<div class="container">';
				$related.=h3($i18n['related'],'class=text-center').br();
				$related.=$rel->get_html();
				$related.='</div>';
				$related.='</div>';
				$related.='<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-580a7aeb4856b9fa"></script> ';
			}else $content.=$div->get_html();
		}else{
			header("HTTP/1.0 404 Not Found");
			$title='404: '.$i18n['not_found'];
			$content=h1($i18n['not_found'], 'class=text-center').p('404', 'class=text-center').br().br();
		}
	break;

	case 'addtocart': //
		$post = [
	    'email' => $config['pagseguro_email'],
	    'token' => $config['pagseguro_token'],
	    'currency' => 'BRL',
	    'charset'  => 'UTF-8',
	    'tipo'  => 'CBR',
	    'itemId1'  => $_GET['itemId'],
	    'itemDescription1'  => $_GET['itemDescription'],
	    'itemAmount1'  => $_GET['itemAmount'],
	    'itemQuantity1'  => $_GET['itemQuantity'],
	    'itemWeight1'  => $_GET['itemWeight']
		];
		//print_r($post);exit;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://ws.pagseguro.uol.com.br/v2/checkout/');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, true);
		// execute!
		$response = curl_exec($ch);

		// close the connection, release resources used
		curl_close($ch);
		//print_r($response);exit;
		if($response != 'Unauthorized'){
			$xml= simplexml_load_string($response);
			if(count($xml->error) == 0){
				$code=$xml->code;
				//$_SESSION['code']=$code;
				//die('<a href="https://pagseguro.uol.com.br/v2/checkout/cart.html?action=view">pagSeguro</a>');
				header('location:https://pagseguro.uol.com.br/v2/checkout/payment.html?code='.$code);
			}
		}
	exit;

	case 'contact':
	case 'contato':
	$js[4]='inc/jquery.validate.bootstrap.min.js';
		$title = 'Contato';
		$c=get('Contato');
		$title = $c['titulo'];


		$content .= h1($title);

		if(isset($_GET['fail'])) $content .= p(strong($i18n['error_title']).'! '.$i18n['error_text'], 'class=alert alert-warning');
		else if(isset($_GET['spam'])) $content .= p(strong($i18n['error_title']).'! '.$i18n['spam'], 'class=alert alert-warning');
		else if(isset($_GET['sent'])) $content .= p(strong($i18n['success_title']).'! '.$i18n['success_text'], 'class=alert alert-success');

		$content .= br().'<div class="row">';
		$content .= '<div class="col-sm-6">';
    include ('inc/form.php');
		$content .= '</div>';
		$content .= '<div class="col-sm-6">';
		$content .= responsive($c['texto']);
		$content .= '</div>';
		$content .= '</div>';
		$content.=div( $c['mapa'] ,'class=mapa');
	break;
	case 'post':
		$pages=pages('data/Blog.sup');
		$filename='data/Blog.sup/'.$pages[$_REQUEST['link']].'.one';
		if(postComment($filename))
			header('location:../sup/'.$_REQUEST['link']);
	exit;
	case 'sendmail':
		include 'inc/sendmail.php';
	exit;
	case 'busca':
		$title='Resultados da busca ';
		if(isset($_GET['q'])){
			$title.='&ldquo;'.$_GET['q'].'&rdquo;';
			$q=$_GET['q'];
		}
		$content=h1($title);
		$content.=div("<gcse:searchresults-only></gcse:searchresults-only>", 'class=text-left');
	break;
	case 'obrigado':
		$title=$i18n['thankyou'];
		$content.=h1($title, 'class=text-center');
		$content.=p($i18n['thankyou_text'], 'class=text-center').br().br();
		$content.=p(img('src=img/boxes.jpg&alt=boxes&class=img-responsive img-rounded center-block'));
	break;

	default:
	case '404':
		if(file_exists('data/'.saEncode($pg).'.one')){
			$default=saEncode($pg);
			break;
		}
		header("HTTP/1.0 404 Not Found");
		$title='404: '.$i18n['not_found'];
		$content=h1($i18n['not_found'], 'class=text-center').p('404', 'class=text-center').br().br();
	break;
}

if($default){ // default
	$c=get('data/'.$default.'.one');
	//print_r($c);exit;
	$title = $c['titulo'];
	$content .= h1($title);

	$r=new div('class=row text-left');
	if($c['foto']){
		$feature='thumb900/'.str_urlencode($c['foto']);
		$r->add(div(img('class=img-responsive&src=thumb500/'.$c['foto']), 'class=col-sm-5'));
		$r->add(div(responsive($c['texto']), 'class=col-sm-7'));
	}else{
		$r->add(div($c['texto'], 'class=col-sm-12'));
	}
	$content.= $r->get_html();
}
/******************** Menu ********************/

$cart= '<form target="pagseguro" action="https://pagseguro.uol.com.br/security/webpagamentos/webpagto.aspx" method="post">
<input type="hidden" name="email_cobranca" value="'.$config['pagseguro_email'].'" />
<input type="hidden" name="tipo" value="VER" />
<button type="submit" name="submit" class="btn btn-link cart">
'.icon('glyphicon glyphicon-shopping-cart').' '.$i18n['cart'].'</button>
</form>';

$sitemap=new sitemap();
$sitemap->add_url($base, 'weekly',1);

$topo=get('Menu_topo');
$menu=new ul('class=nav nav-pills navbar-nav navbar-right');
foreach($topo as $t){
	if($t['ocultar']) continue;
	$menu->add_li(a($t['nome'], 'href='.$t['link']));
	$sitemap->add_url($base.$t['link'], 'monthly', 0.6);
}
//$menu->add_li(a($i18n['quemsomos'], 'href=quem-somos'));
//$menu->add_li(a($i18n['produtos'].' <i class="caret"></i>', 'href=#&class=dropdown-toggle&data-toggle=dropdown&role=button&aria-expanded=false').$sub1->get_html());
//$menu->add_li(a($i18n['fotos'], 'href=fotos'));
//$menu->add_li(a($i18n['noticias'], 'href=noticias'));
//$menu->add_li(a($i18n['contact'], 'href=contato'));
if($config['ecommerce']) $menu->add_li($cart);
$nav=$menu->get_html();

//$sitemap->add_url($base.'noticias', 'daily', 0.9);
//$sitemap->add_url($base.'fotos', 'monthly',0.6);

ksort($paginas);
$sub1=new ul('class=nav nav-pills navbar-nav navbar-center');
foreach($paginas as $categoria){
	if($categoria['modo']=='Ocultar')continue;
	if(substr($categoria['link'],0,4)=='http')$link=$categoria['link'];
	else $link='produtos/'.$categoria['link'];
	$sub1->add_li(a($categoria['categoria'],'href='.$link));
	$sitemap->add_url($base.$link, 'monthly',1);
}
$nav.=$sub1->get_html();

// sitemap.xml
// freq: always, hourly, daily, weekly, monthly, yearly, never
// priority: 0-1
//$sitemap->add_url($base.'produtos', 'monthly',1);
$sitemap->add_url($base.'quem-somos', 'yearly',0.8);
$sitemap->add_url($base.'contato', 'yearly',0.5);
$sitemapXML=$sitemap->get_xml();


/****************** Cart *******************/

function pagSeguro($descr,$val, $weight=300, $id){
	global $config;
	if($config['frete_gratis'] || isset($_GET['freeshipping']))$weight=0;
	global $config;
  //https://pagseguro.uol.com.br/v2/checkout/payment.html
	$o='<form target="pagseguro" method="post" action="https://pagseguro.uol.com.br/v2/checkout/cart.html?action=add"> ';
    $o.='<!-- Campos obrigatórios -->
    <input type="hidden" name="receiverEmail" value="'.$config['pagseguro_email'].'">
    <input type="hidden" name="currency" value="BRL">

    <!-- Itens do pagamento (ao menos um item é obrigatório) -->
    <input type="hidden" name="itemId" value="'.$id.'">
    <input type="hidden" name="itemDescription" value="'.$descr.'">
    <input type="hidden" name="itemAmount" value="'.$val.'">
    <input type="hidden" name="itemQuantity" value="1">
    <input type="hidden" name="itemWeight" value="'.$weight.'">

    <input type="hidden" name="shippingType" value="3">

    <!-- Código de referência do pagamento no seu sistema (opcional) -->
    <input type="hidden" name="reference" value="'.$descr.'">
    <input type="hidden" name="encoding" value="UTF-8">

    <!-- submit do form (obrigatório) -->
    <button class="btn-buy" type="submit" name="submit">
    <img src="img/comprar.png" srcset="img/comprar@2x.png 2x" class="img-responsive"
    alt="Pague com PagSeguro" title="Compra 100% segura"></button>
		</form> ';
		//$o.='<a href="https://pagseguro.uol.com.br/desenvolvedor/simulador_de_frete.jhtml?CepOrigem=13085309&amp;Peso='.($weight/10).'&amp;Valor='.str_replace('.',',',$val).'" id="ps_freight_simulator" target="_blank"><img src="https://p.simg.uol.com.br/out/pagseguro/i/user/imgCalculoFrete.gif" id="imgCalculoFrete" alt="Cálculo automático de frete" border="0" /></a><script type="text/javascript" src="https://p.simg.uol.com.br/out/pagseguro/j/simulador_de_frete.js"></script>';
		return $o;
}
/****************** Footer *******************/
$description=str_replace(array("\r\n", "\r", "\n"), "", htmlspecialchars(strip_tags($description)));
$footer='';
$urlsplit=explode('?',$_SERVER['REQUEST_URI']);
$pageurl=$urlsplit[0];
// parceiros
$parceiros=get('Parceiros');
if(count($parceiros)>0){
	$par=new carousel('parceria', false, false);
	$item='';
	$i=0;
	foreach ($parceiros as $parceiro) {
		$item.=' '.a(img('src=thumb180/'.$parceiro['logo']),'class=parceiro&href='.$parceiro['link']);
		$i++;
		if($i>=6){
			$par->add($item);
			$item='';
			$i=0;
		}
	}
	if($i && $i<6)$par->add($item);
	$footer.=h3($i18n['parceiros'],'class=text-muted');
	$footer.=$par->get_html();
}

$footer.='<div class="fineprint">';
$footer.=$i18n['footer'].'. ';
//$footer.=$i18n['credit'].' '.a('Oktala','href=http://www.oktala.com.br/').'.';
$footer.= ' &nbsp; <a href="http://www.getsiteadmin.com/"><img alt="siteAdmin" src="img/badge.gif" /></a>';

$footer.=' &nbsp;&nbsp; '.a(icon('glyphicon glyphicon-lock').'&nbsp;Login','href=siteadmin');

if($langSwitcher){
	if($lang=='en') $footer.=' &nbsp;&nbsp; '.a(icon('glyphicon glyphicon-globe').'&nbsp;Português','href='.$pageurl.'?lang\=pt');
	else $footer.=' &nbsp;&nbsp; '.a(icon('glyphicon glyphicon-globe').'&nbsp;English','href='.$pageurl.'?lang\=en');
}
$footer.='</div>';

	$opengraph=
	'<meta property="og:image" content="'.$base.$feature.'" />
	<meta property="og:url" content="http://'.$_SERVER['HTTP_HOST'].$pageurl.'?lang='.$lang.'" />
	<meta property="og:title" content="'.$title.'" />
	<meta property="og:description" content="'.$description.'" />
	<meta property="og:locale" content="'.$lang_flag.'" />
	<meta property="og:type" content="website" />';
    /*/ Twitter Cards
    <meta name="twitter:card" content="summary">
    //<meta name="twitter:site" content="@nytimesbits">
    //<meta name="twitter:creator" content="@joaomak">
    */

    // MS Start Page Icon
    /*<meta name="msapplication-TileColor"
    content="#123456"/>
<meta name="msapplication-TileImage"
    content="your_logo.png"/>*/

if(isset($_SERVER['REDIRECT_URL']))$canonical= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REDIRECT_URL'].'?lang='.$lang;

    ?>
