<?php put('mensagem'); ?>

<nav id="nav" class="navbar navbar-default navbar-static-top" >
   <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs">
          <span class="sr-only">Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="."><?php put('logo'); ?></a>
    </div>
    <div class="collapse navbar-collapse" id="bs">
     <?php put('nav') ?>
    </div>
  </div>
</nav>


<div id="content" >
  <?php put('carousel'); ?>
  <div class="container">
  <?php put('content'); ?>
  </div>
  <?php put('related'); ?>
</div>

<footer id="footer">
  <?php put('footer'); ?>
</footer>

<?php put('body'); ?>
