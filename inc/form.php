<?php
// contact form
$form=new form('method=post&action=sendmail&id=contact-form&accept-charset=UTF-8&role=form');

$form->add(div(
	label($i18n['name'], 'for=name&class=control-label')
	.input('type=text&name=name&id=name&class=form-control&required=required')
,'class=form-group'));

$form->add(div(
	label($i18n['your_email'], 'for=email&class=control-label')
	.input('type=email&name=email&id=email&class=form-control&required=required')
,'class=form-group'));

$form->add(div(
	label($i18n['phone'], 'for=fone&class=control-label')
	.input('type=text&name=fone&id=fone&class=form-control')
,'class=form-group'));

$form->add(div(
	label($i18n['message'], 'for=mensagem&class=control-label')
	.textarea('','type=text&name=mensagem&id=mensagem&class=form-control&required=required')
,'class=form-group'));


$form->add('<div class="row">');

// Captcha
include_once('siteadmin/lib/captcha/captcha.php');
captchaSetColor('#555555');
captchaSetBgColor('#ffffff');
captchaSetFontSize('24');
captchaSetFontFile('Myndrain.ttf');
$form->add(div('
	<div class="input-group">
	<span class="input-group-addon"><img id="captcha-img" src="siteadmin/captcha/'. captcha().'" height="32" /></span>
	<label for="captcha" class="control-label">'. $i18n['captcha'].'</label>
	<input type="text" name="captcha" id="captcha" size="4" required="required" class="form-control" /></div>'
,'class=form-group col-sm-9&id=captcha-block'));

$form->add( div(
	input('class=btn btn-primary btn-block&type=submit&value='. $i18n['send'])
	// spam control
	.input('type=text&value=&name=url&style=display:none')
,'class=col-sm-3'));

$form->add('</div>');

$content.=$form->get_html();

?>
