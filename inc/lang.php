<?php

$lang='en';

// change language
if(isset($_GET['lang'])){
	setcookie("lang", $_GET['lang'], 2147483647,'/');  // six months: 15778458
	//header ('location:'.'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REDIRECT_URL']);
	//exit;
}
//$lang='pt';
if(isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
	if(substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2)=='pt')
		$lang='pt';

if(isset($_COOKIE['lang']))$lang=$_COOKIE['lang'];
if(isset($_GET['lang'])) $lang=$_GET['lang'];

//$lang='pt';
$langSwitcher=true;


$lang_flag=$lang;
if($lang=='pt')$lang_flag='pt_BR';
if($lang=='en')$lang_flag='en_US';

$RFC5646=strtolower(str_replace('_','-',$lang_flag));

setlocale(LC_ALL, $lang_flag);
define("LANG", $lang);

$i18n=get('Translate');

?>
