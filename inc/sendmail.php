<?php

//$to = 'email@cliente';
//$to ='joaomak@gmail.com';
$to = $config['pagseguro_email'];

if(isset($_REQUEST['email'])){

	if(isset($_REQUEST['captcha'])) HumanCheck($_REQUEST['captcha']);

	if (isset($_POST)){
			//Preventing Email Header Injection in PHP
			$_SAFE_POST = array_map('clean_user_input', $_POST);

            $body = '';
            $subject  = "[".$_SERVER['SERVER_NAME']."] ";
            if(isset($_REQUEST['subject']))$subject .=$_REQUEST['subject'];
            else $subject .= date("m/d/Y")."\r\n";
            //Iterate through all the POSTed variables, and add them to the message body.

			if(!isset($_REQUEST['name']))$_REQUEST['name']='';
			$from = $_REQUEST['name'].'<'.$_REQUEST['email'].'>';

            foreach($_SAFE_POST as $key=>$value){
                if(strlen($value)>0 && $key!='required' && $key!='x' && $key!='y' && $key!='captcha' && $key!='subject')
                    $body .= str_replace('_',' ',$key) . ": " . ($value) . "\r\n\n";
            }
//echo $body; exit;

            //Build up some nice From/Reply Headers
            $headers = "From: $from\r\n";
            $headers .= "Reply-To: $from\r\n";
            //Mail the message out.
            //Requires setting php3.ini sendmail path as per instructions
            $success = mail($to, $subject, utf8_decode($body), $headers);
            //Always check return codes from functions.
            if ($success){
                header('Location:contato?sent');
                exit;
            }

        }

}
header('Location:contato?fail');

function HumanCheck($cap){

	session_start();
	if (isset($_SESSION['captcha']) && strtolower(trim($cap)) == strtolower($_SESSION['captcha']) && $_REQUEST['url']==='') return true;
	else {
		header('Location:contato?spam');
		exit;
	}

}
function clean_user_input($value) {
	if (stristr($value, 'content-type')) return '';
	if (stristr($value, 'bcc:')) return '';
	if (stristr($value, 'to:')) return '';
	if (stristr($value, 'cc:')) return '';
	if (stristr($value, 'href')) return '';

	//strip the quotes, but only if magic quotes are on.
	if (get_magic_quotes_gpc()) $value = stripslashes($value);

	//replace any newline characters with blank spaces.
	$value = str_replace(array( "\r", "\n", "%0a", "%0d"), '', $value);

	return trim($value);
}

?>
