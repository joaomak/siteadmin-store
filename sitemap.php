<?php
// sitemap.xml
include ("inc/pages.php");
header('Content-type: text/xml; charset=utf-8');
header("Pragma: no-cache");
echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?> ";
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
if(isset($sitemapXML)) echo $sitemapXML;
echo '</urlset>';
?>
