<?php
include ("inc/pages.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="<?php echo $RFC5646; ?>"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="<?php echo $RFC5646; ?>"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="<?php echo $RFC5646; ?>"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="<?php echo $RFC5646; ?>"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="<?php echo $RFC5646; ?>"> <!--<![endif]-->
<!-- Developed by Oktala - www.oktala.com.br -->
<head>
	<meta charset="utf-8" />
    <base href="<?php put('base') ?>" />
    <title><?php if(isset($title))echo strip_tags($title).$fixedTitle;?> </title>
	  <meta name="title" content="<?php if(isset($title))echo strip_tags($title).$fixedTitle;?>" />
    <meta name="description" content="<?php echo strip_tags($description); ?>" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="canonical" href="<?php put('canonical'); ?>" />
   <?php
    if(isset($cssf)){
    foreach($cssf as $cssfile) echo '<link href="'.linkFile($cssfile).'" rel="stylesheet" type="text/css" />'."\n";
    }
  ?>
<style type="text/css" media="all">
<?php echo $css; ?>

</style>
<script type="text/javascript" src="inc/prefixfree.min.js"></script>
<?php put('opengraph'); ?>
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
		<?php put('head_extra'); ?>
</head>

<body id="<?php echo $pg; ?>" class="<?php put('lang') ?>">
<?php include('inc/'.$rootTemplate); ?>

<?php
    if(isset($js)){
	  ksort($js);
	  foreach($js as $jsfile) echo '<script type="text/javascript" src="'.$jsfile.'"></script>'."\n";
    }
?>
</body>
</html>
