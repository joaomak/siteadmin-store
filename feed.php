<?php
$_GET['pg']='feed';
include ("inc/pages.php");  
header('Content-type: text/xml; charset=utf-8');
header("Pragma: no-cache");

echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?> ";

?>

<rss version="0.92">
	<channel>
		<title><?php if(isset($title))echo strip_tags($title).$fixedTitle;?></title>
		<link>http://<?php echo $_SERVER['SERVER_NAME']; ?></link>
		<description><?php if(isset($description))echo ($description); ?></description>
		<language><?php echo str_replace('_','-',strtolower($lang)); ?></language>

<?php	if(isset($content)) echo $content; ?>

	</channel>
</rss>
